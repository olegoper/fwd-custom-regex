package com.ex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PatternBuilder {
    private static final HashSet<Character> SPECIAL_SYMBOLS = new HashSet<>();
    private static final HashMap<Character, Repeats> REPEATS_TABLE = new HashMap<>();
    private String pattern;
    private ArrayList<ExpressionMatcher> matchers;

    private static class Repeats {
        private int min;
        private int max;

        public Repeats(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public int getMin() {
            return min;
        }

        public int getMax() {
            return max;
        }
    }

    static {
        SPECIAL_SYMBOLS.add('.');
        SPECIAL_SYMBOLS.add('*');
        SPECIAL_SYMBOLS.add('+');
        SPECIAL_SYMBOLS.add('?');

        REPEATS_TABLE.put('*', new Repeats(0, -1));
        REPEATS_TABLE.put('+', new Repeats(1, -1));
        REPEATS_TABLE.put('?', new Repeats(0, 1));
    }

    public PatternBuilder(String pattern) {
        this.pattern = pattern;
    }

    public List<ExpressionMatcher> build() {
        matchers = new ArrayList<>();
        StringBuilder text = new StringBuilder();

        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);

            if (c == '\\') {
                i += processEscapeCharacter(text, i + 1);
                continue;
            }

            if (c == '[') {
                addText(text);
                i += processRange(i + 1);
                continue;
            }

            Repeats repeats = getRepeats(i);

            if (c == '.') {
                addText(text);
                addAny(repeats);
            } else {
                if (repeats != null) {
                    addText(text);
                    addChar(c, repeats);
                } else {
                    text.append(c);
                }
            }

            if (repeats != null) {
                i++;
            }
        }

        addText(text);
        return matchers;
    }

    private void addText(StringBuilder builder) {
        if (builder.length() == 0) {
            return;
        }
        matchers.add(new TextMatcher(builder.toString()));
        builder.setLength(0);
    }

    private void addAny(Repeats repeats) {
        matchers.add(withRepeats(new SymbolMatcher(SymbolMatcher.Type.ANY), repeats));
    }

    private void addSpace(Repeats repeats) {
        matchers.add(withRepeats(new SymbolMatcher(SymbolMatcher.Type.SPACE), repeats));
    }

    private void addNotSpace(Repeats repeats) {
        matchers.add(withRepeats(new SymbolMatcher(SymbolMatcher.Type.NOT_SPACE), repeats));
    }

    private void addChar(char c, Repeats repeats) {
        matchers.add(withRepeats(new TextMatcher(String.valueOf(c)), repeats));
    }

    private ExpressionMatcher withRepeats(ExpressionMatcher matcher, Repeats repeats) {
        if (repeats == null) {
            return matcher;
        }
        RepeatedExpressionMatcher repeatedExpressionMatcher = new RepeatedExpressionMatcher(
                matcher, repeats.getMin(), repeats.getMax());
        return repeatedExpressionMatcher;
    }

    int processEscapeCharacter(StringBuilder text, int i) {
        if (i >= pattern.length()) {
            throw new InvalidPatternException("Bad character at position " + (i - 1));
        }

        char c = pattern.charAt(i);
        if (SPECIAL_SYMBOLS.contains(c)) {
            Repeats repeats = getRepeats(i);
            if (repeats != null) {
                addText(text);
                addChar(c, repeats);
                return 2;
            } else {
                text.append(c);
                return 1;
            }
        }

        addText(text);

        Repeats repeats = getRepeats(i);

        if (c == 's') {
            addSpace(repeats);
        } else if (c == 'S') {
            addNotSpace(repeats);
        } else {
            throw new InvalidPatternException("Bad character at position " + i);
        }


        if (repeats != null) {
            return 2;
        }
        return 1;
    }

    private int processRange(int i) {
        int end = pattern.indexOf(']', i);
        if (end < 0) {
            throw new InvalidPatternException("Can't find boundaries for range expression at " + i);
        }

        Repeats repeats = getRepeats(end);

        matchers.add(withRepeats(new TextRangeMatcher(pattern.substring(i, end)), repeats));

        int len = end - i + 1;
        if (repeats != null) {
            len += 1;
        }

        return len;
    }

    private Repeats getRepeats(int i) {
        if (i + 1 >= pattern.length()) {
            return null;
        }
        return REPEATS_TABLE.get(pattern.charAt(i + 1));
    }
}
