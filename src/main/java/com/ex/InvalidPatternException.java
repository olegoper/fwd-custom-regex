package com.ex;

public class InvalidPatternException extends RuntimeException {
    public InvalidPatternException(String message) {
        super(message);
    }
}
