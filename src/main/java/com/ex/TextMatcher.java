package com.ex;

public class TextMatcher extends ExpressionMatcher {
    private String text;

    public TextMatcher(String text) {
        this.text = text;
    }

    @Override
    public int match(String expression, int startIndex) {
        if (text.length() > expression.length() - startIndex) {
            return -1;
        }
        if (text.equals(expression.substring(startIndex, startIndex + text.length()))) {
            return startIndex + text.length();
        }
        return -1;
    }
}

