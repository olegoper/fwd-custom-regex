package com.ex;

public class SimpleMatcher {
    private String pattern;

    public SimpleMatcher(String pattern) {
        this.pattern = pattern;
    }

    public boolean apply(String expression) {
        return match(expression, 0, 0);
    }

    private int minRepeats(int i) {
        if (i + 1 >= pattern.length()) {
            return -1;
        }
        char nextChar = pattern.charAt(i + 1);
        if (nextChar == '*') {
            //System.out.println("=*=");
            return 0;
        }
        if (nextChar == '+') {
            return 1;
        }
        return -1;
    }

    private boolean match(String expr, int i, int j) {
        while (i < pattern.length() && j < expr.length()) {
            int repeats = minRepeats(i);
            if (repeats == 0) {
                if (match(expr, i + 2, j)) {
                    return true;
                }
            }
            if (pattern.charAt(i) != '.' && pattern.charAt(i) != expr.charAt(j)) {
                return false;
            } else {
                // System.out.println("Match " + expr.charAt(j));
            }
            if (repeats >= 0) {
                return match(expr, i, j + 1) || match(expr, i + 2, j);
            }
            i++;
            j++;
        }

        while (minRepeats(i) == 0) {
            i += 2;
        }

        // System.out.println("I= " + i + " J= " + j);
        if (i == pattern.length() && j == expr.length()) {
            return true;
        }
        return false;
    }
}
