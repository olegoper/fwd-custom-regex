package com.ex;

public class RepeatedExpressionMatcher extends ExpressionMatcher {
    private int minRepeat = -1;
    private int maxRepeat = -1;
    private ExpressionMatcher matcher;

    public RepeatedExpressionMatcher(ExpressionMatcher matcher, int minRepeat, int maxRepeat) {
        if (maxRepeat >= 0 && minRepeat >= maxRepeat) {
            throw new IllegalArgumentException("maxRepeat should be greater than minRepeat");
        }
        this.minRepeat = minRepeat;
        this.maxRepeat = maxRepeat;
        this.matcher = matcher;
    }

    public int getMinRepeat() {
        return minRepeat;
    }

    public int getMaxRepeat() {
        return maxRepeat;
    }

    public int matchNext(String expression, int startIndex) {
        return matcher.match(expression, startIndex);
    }

    @Override
    public int match(String expression, int startIndex) {
        throw new UnsupportedOperationException();
    }
}
