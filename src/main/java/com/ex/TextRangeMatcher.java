package com.ex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TextRangeMatcher extends ExpressionMatcher {
    private HashSet<Character> characters = new HashSet<>();
    private List<Range> ranges = new ArrayList<>();

    private static class Range {
        private char start;
        private char end;

        public Range(char start, char end) {
            this.start = start;
            this.end = end;
        }

        public char getStart() {
            return start;
        }

        public char getEnd() {
            return end;
        }
    }

    public TextRangeMatcher(String pattern) {
        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            if (i + 1 < pattern.length() && c == '\\') {
                addCharacter(pattern.charAt(i + 1), true);
                i += 1;
                continue;
            }
            if (i + 2 < pattern.length() && pattern.charAt(i + 1) == '-') {
                addRange(c, pattern.charAt(i + 2));
                i += 2;
                continue;
            }
            addCharacter(c, false);
        }
    }

    @Override
    public int match(String expression, int startIndex) {
        if (expression.length() <= startIndex) {
            return -1;
        }
        char c = expression.charAt(startIndex);
        if (characters.contains(c)) {
            return startIndex + 1;
        }
        for (Range range : ranges) {
            if (range.getStart() <= c && c <= range.getEnd()) {
                return startIndex + 1;
            }

        }
        return -1;
    }

    private boolean isValid(char c) {
        return Character.isLetter(c) || Character.isDigit(c);
    }

    private void addRange(char start, char end) {
        boolean validTypes = (Character.isDigit(start) && Character.isDigit(end))
                || (Character.isLetter(start) && Character.isLetter(end));
        boolean isSameCase = Character.isLowerCase(start) == Character.isLowerCase(end);
        if (!validTypes || !isSameCase || !(end > start)) {
            throw new InvalidPatternException("Bad range pattern for " + start + "-" + end);
        }
        ranges.add(new Range(start, end));
    }

    private void addCharacter(char c, boolean isEscaped) {
        if (isValid(c)) {
            if (isEscaped) {
                throw new InvalidPatternException("Bad range pattern for " + c);
            }
        } else {
            if (!isEscaped) {
                throw new InvalidPatternException("Bad range pattern for " + c);
            }
        }
        characters.add(c);
    }
}
