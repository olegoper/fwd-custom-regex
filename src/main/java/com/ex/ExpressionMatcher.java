package com.ex;

public abstract class ExpressionMatcher {
    abstract public int match(String expression, int startIndex);
}
