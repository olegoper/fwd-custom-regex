package com.ex;

public class SymbolMatcher extends ExpressionMatcher {
    private Type type;

    public enum Type {
        ANY,
        SPACE,
        NOT_SPACE
    }

    public SymbolMatcher(Type type) {
        this.type = type;
    }

    @Override
    public int match(String expression, int startIndex) {
        switch (type) {
            case ANY:
                return matchAny(expression, startIndex);
            case SPACE:
                return matchSpace(expression, startIndex);
            case NOT_SPACE:
                return matchNotSpace(expression, startIndex);
            default:
                throw new IllegalArgumentException("Unsupported type " + type);
        }
    }

    private int matchAny(String expression, int startIndex) {
        if (expression.length() > startIndex) {
            return startIndex + 1;

        }
        return -1;
    }

    private int matchSpace(String expression, int startIndex) {
        if (expression.length() > startIndex && Character.isWhitespace(expression.charAt(startIndex))) {
            return startIndex + 1;
        }
        return -1;
    }

    private int matchNotSpace(String expression, int startIndex) {
        if (expression.length() > startIndex && !Character.isWhitespace(expression.charAt(startIndex))) {
            return startIndex + 1;
        }
        return -1;
    }

}
