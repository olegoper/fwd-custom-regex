package com.ex;

import java.util.List;

public class RegexMatcher {
    private List<ExpressionMatcher> matchers;

    public RegexMatcher(String pattern) {
        this.matchers = new PatternBuilder(pattern).build();
    }

    public boolean apply(String expression) {
        return match(expression, matchers, 0, 0);
    }

    private boolean match(String expr, List<ExpressionMatcher> matchers, int charIndex, int matcherIndex) {
        while (matchers.size() > matcherIndex) {
            ExpressionMatcher nextMatcher = matchers.get(matcherIndex);
            if (nextMatcher instanceof RepeatedExpressionMatcher) {
                return matchRepeated(expr, matchers, charIndex, matcherIndex);
            }
            charIndex = nextMatcher.match(expr, charIndex);
            matcherIndex++;
            if (charIndex < 0) {
                return false;
            }
        }

        if (charIndex == expr.length() && matcherIndex == matchers.size()) {
            return true;
        }
        return false;
    }

    private boolean matchRepeated(
            String expr,
            List<ExpressionMatcher> matchers,
            int startIndex,
            int matcherIndex)
    {
        RepeatedExpressionMatcher matcher = (RepeatedExpressionMatcher) matchers.get(matcherIndex);
        int repeats = 0;

        while (expr.length() > startIndex && startIndex >= 0 && repeats < matcher.getMinRepeat()) {
            startIndex = matcher.matchNext(expr, startIndex);
            repeats++;
        }
        while (startIndex >= 0) {
            if (match(expr, matchers, startIndex, matcherIndex + 1)) {
                return true;
            }

            if (matcher.getMaxRepeat() >= 0 && repeats >= matcher.getMaxRepeat()) {
                break;
            }
            repeats++;
            startIndex = matcher.matchNext(expr, startIndex);
            if (startIndex < 0) {
                return false;
            }
        }

        matcherIndex++;
        if (startIndex == expr.length() && matcherIndex == matchers.size()) {
            return true;
        }
        return false;
    }
}
