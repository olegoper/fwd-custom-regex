package com.ex;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RegexMatcherTest {
    public void checkPattern(String pattern, String expr, boolean shouldMatch) {
        RegexMatcher matcher = new RegexMatcher(pattern);
        boolean res = matcher.apply(expr);
        if (!shouldMatch) {
            res = !res;
        }
        assertTrue(res);
    }

    @Test
    public void simpleStringExactMatchCorrect() {
        checkPattern("Hello World!", "Hello World!", true);
    }

    @Test
    public void simpleStringExactMatchFails() {
        checkPattern("Hello World!", "Bye World!", false);
    }

    @Test
    public void dotStartMatchInBeginAndEndCorrect() {
        checkPattern(".*ello.*", "Hello World!", true);
    }

    @Test
    public void plusStartMatchInBeginAndEndCorrect() {
        checkPattern(".+@.+", "me@mail.ru", true);
    }

    @Test
    public void dotMatchFails() {
        checkPattern(".+@.+\\..+", "me@mailru", false);
    }

    @Test
    public void dotMatchCorrect() {
        checkPattern(".+@.+\\..+", "me@mail.ru", true);
    }

    @Test
    public void questionMarkSingleMatchCorrect() {
        checkPattern("maybe?", "maybe", true);
    }

    @Test
    public void questionMarkZeroMatchCorrect() {
        checkPattern("maybe?", "mayb", true);
    }

    @Test
    public void questionMarkDoubleMatchFails() {
        checkPattern("maybe?", "maybee", false);
    }

    @Test
    public void noSpaceStringCorrect() {
        checkPattern("\\S+", "maybee", true);
    }

    @Test
    public void noSpaceStringFails() {
        checkPattern("\\S+", "  maybee", false);
    }

    @Test
    public void spaceAndNoSpaceStringCorrect() {
        checkPattern("\\s+\\S+", "    maybee", true);
    }

    @Test
    public void urlMatches() {
        checkPattern("http://[a-zA-Z0-9\\_\\-]+\\.[a-zA-Z0-9\\_\\-]+\\.[a-zA-Z0-9\\_\\-]+",
                "http://www.google.com", true);
    }

    @Test
    public void floatingPointMatches() {
        String pattern = "[0-9]+\\.?[0-9]*";
        checkPattern(pattern, "3.14", true);
        checkPattern(pattern, "3.", true);
        checkPattern(pattern, "0.14", true);
    }

    @Test
    public void hexValuesMatches() {
        String pattern = "0x[a-zA-Z0-9][a-zA-Z0-9]?[a-zA-Z0-9]?[a-zA-Z0-9]?";
        checkPattern(pattern, "0x100", true);
        checkPattern(pattern, "0xDEAF", true);
    }

    @Test
    public void identifiersMatches() {
        String pattern = ".*args.*";
        checkPattern(pattern, "public static void main(String[] args)", true);
    }

    @Test
    public void singleLineCommentsMatches() {
        String pattern = "\\s*//.*";
        checkPattern(pattern, "  // commented dead code here", true);
        checkPattern(pattern, "  /* but not here", false);
    }

}
