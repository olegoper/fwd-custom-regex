package com.ex;

import org.junit.Test;

    import static org.junit.Assert.assertTrue;

public class SymbolMatcherTest {
    @Test
    public void anyMatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.ANY);
        assertTrue(matcher.match("a", 0) == 1);
    }

    @Test
    public void anyWithShiftMatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.ANY);
        assertTrue(matcher.match(" a ", 1) == 2);
    }

    @Test
    public void spaceMatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.SPACE);
        assertTrue(matcher.match(" ", 0) == 1);
    }

    @Test
    public void spaceMismatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.SPACE);
        assertTrue(matcher.match("1", 0) == -1);
    }

    @Test
    public void spaceWithShiftMatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.SPACE);
        assertTrue(matcher.match("s 5", 1) == 2);
    }

    @Test
    public void notSpaceMatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.NOT_SPACE);
        assertTrue(matcher.match("5", 0) == 1);
    }

    @Test
    public void notSpaceMismatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.NOT_SPACE);
        assertTrue(matcher.match(" ", 0) == -1);
    }

    @Test
    public void notSpaceWithShiftMatch() {
        SymbolMatcher matcher = new SymbolMatcher(SymbolMatcher.Type.NOT_SPACE);
        assertTrue(matcher.match(" 5 ", 1) == 2);
    }
}
