package com.ex;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TextMatcherTest {
    @Test
    public void textMatch() {
        String pattern = "Test text";
        TextMatcher matcher = new TextMatcher(pattern);
        assertTrue(matcher.match(pattern, 0) == pattern.length());
    }

    @Test
    public void textMismatch() {
        String pattern = "Test text";
        TextMatcher matcher = new TextMatcher(pattern);
        assertTrue(matcher.match("Another text", 0) == -1);
    }

    @Test
    public void textWithShiftMatch() {
        String pattern = "Test text";
        TextMatcher matcher = new TextMatcher(pattern);
        assertTrue(matcher.match("  Test text here", 2) == pattern.length() + 2);
    }
}
